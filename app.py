##
# A simple image signature knn example
#
# Author: Fabrício G. M. de Carvalho, Ph.D.
##
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from copy import deepcopy
from model import img_signature, distance
from knn import knn

NF = 255  # normalization factor
K = 15  # algorithm parameter

# image file reading
# templates
backgrounds = mpimg.imread('clouds/background_example_reduced.bmp')
clouds = mpimg.imread('clouds/cloud_example_03_reduced.bmp')

# unknown image
unclassified = mpimg.imread('clouds/unknown_reduced.bmp')

#image properties
ref_img_1_prop = {
    'img': backgrounds,
    'class': 'background',
    'rows': len(backgrounds),
    'columns': len(backgrounds[0])
}
ref_img_2_prop = {
    'img': clouds,
    'class': 'cloud',
    'rows': len(clouds),
    'columns': len(clouds[0])
}
ref_imgs = [ref_img_1_prop, ref_img_2_prop]

unknown_img_prop = {
    'img': unclassified,
    'class': 'unknown',
    'rows': len(unclassified),
    'columns': len(unclassified[0])
}

## label individual pixel examples:
labeled_examples = []
for ref_img in ref_imgs:
    for x in range(ref_img['rows']):
        for y in range(ref_img['columns']):
            fv = []
            for channel in range(3):
                fv.append(ref_img['img'][x][y][channel] / NF)
            labeled_examples.append({
                'feature_vector': deepcopy(fv),
                'class': ref_img['class']
            })
            print('labeling example (%s, %s)' % (x, y))

new_image = []
yellowCount = 0
cloudCount = 0
groundCount = 0
for x in range(unknown_img_prop['rows']):
    new_image.append([])
    for y in range(unknown_img_prop['columns']):
        print('current row and column: (%s, % s)' % (x, y))
        new_image[x].append([])
        fv = []
        for channel in range(3):
            fv.append(unknown_img_prop['img'][x][y][channel] / NF)
        current_pixel = {'feature_vector': fv, 'class': ''}
        print('classification begins')
        pixel_class = knn(labeled_examples, current_pixel, K)
        print('classification ended')
        if pixel_class == 'background':
            new_image[x][y] = [255, 255, 255]
            groundCount = groundCount + 1
        elif pixel_class == 'cloud':
            new_image[x][y] = [0, 0, 0]
            cloudCount = cloudCount + 1
        else:
            new_image[x][y] = [255, 255, 0]
            yellowCount = yellowCount + 1
        os.system('cls||clear')
        print('Ground Pixels: ', groundCount)
        print('Cloud Pixels: ', cloudCount)
        print('yellow Pixels: ', yellowCount)

#print(new_image[0][0])
print('Ground Pixels: ', groundCount)
print('Cloud Pixels: ', cloudCount)
print('yellow Pixels: ', yellowCount)
print('Total Pixels: ', groundCount + cloudCount + yellowCount)

plt.figure(1)
plt.imshow(new_image)

plt.figure(2)
plt.imshow(unknown_img_prop['img'])
plt.show()
